﻿using UnityEngine;

public class Portal : MonoBehaviour{
    private PlayerController _playerController;
    private PortalRED1       _portalRed1;
    private PortalRED2       _portalRed2;
    private PortalGreen1     _portalGreen1;
    private PortalGreen2     _portalGreen2;
    private PortalFiol1      _portalFiol1;
    private PortalFiol2      _portalFiol2;
    private PortalBiruza1    _portalBiruza1;
    private PortalBiruza2    _portalBiruza2;
    private PortalYelow1     _portalYelow1;
    private PortalYelow2     _portalYelow2;

    // [SerializeField] private Transform _portalRed1;
    // [SerializeField] private Transform _portalRed2;
    // [SerializeField] private Transform _portalGreen1;
    // [SerializeField] private Transform _portalGreen2;
    // [SerializeField] private Transform _portalFiol1;
    // [SerializeField] private Transform _portalFiol2;
    // [SerializeField] private Transform _portalBiruza1;
    // [SerializeField] private Transform _portalBiruza2;

    private Vector2 currientDestenationPortal;

    public bool CheckPortalReady {get; set;}

    private void Update() {
        if(_playerController.PortalReady) {
            ChekPortal();
        }

        ChekPortal();
    }

    private void Start() {
        _playerController = FindObjectOfType<PlayerController>();
        _portalRed1       = FindObjectOfType<PortalRED1>();
        _portalRed2       = FindObjectOfType<PortalRED2>();
        _portalGreen1     = FindObjectOfType<PortalGreen1>();
        _portalGreen2     = FindObjectOfType<PortalGreen2>();
        _portalFiol1      = FindObjectOfType<PortalFiol1>();
        _portalFiol2      = FindObjectOfType<PortalFiol2>();
        _portalBiruza1    = FindObjectOfType<PortalBiruza1>();
        _portalBiruza2    = FindObjectOfType<PortalBiruza2>();
        _portalYelow1     = FindObjectOfType<PortalYelow1>();
        _portalYelow2     = FindObjectOfType<PortalYelow2>();
    }


    private void ChekPortal() {
        int          layerMask       = LayerMask.GetMask("Portal");
        Vector2      chekPortalPoint = transform.position;
        RaycastHit2D hit             = Physics2D.Raycast(chekPortalPoint, Vector2.up, 0.5f, layerMask);
        if(hit.collider != null) {
            if(hit.collider.CompareTag("PortalRed1")) {
                currientDestenationPortal = _portalRed2.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalRed2")) {
                currientDestenationPortal = _portalRed1.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalB1")) {
                currientDestenationPortal = _portalBiruza2.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalB2")) {
                currientDestenationPortal = _portalBiruza1.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalGreen1")) {
                currientDestenationPortal = _portalGreen2.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalGreen2")) {
                currientDestenationPortal = _portalGreen1.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalFiol1")) {
                currientDestenationPortal = _portalFiol2.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalFiol2")) {
                currientDestenationPortal = _portalFiol1.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalYelow1")) {
                currientDestenationPortal = _portalYelow2.transform.position;
                CheckPortalReady          = true;
            }

            if(hit.collider.CompareTag("PortalYelow2")) {
                currientDestenationPortal = _portalYelow1.transform.position;
                CheckPortalReady          = true;
            }
        }
        else {
            CheckPortalReady = false;
        }
    }

    public void SetToPortalValue() {
        if(_playerController.PortalReady && CheckPortalReady) {
            _playerController.PortaActivated(currientDestenationPortal);
        }
    }
}
