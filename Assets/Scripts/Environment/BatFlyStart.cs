﻿using System.Collections;
using UnityEngine;

public class BatFlyStart : MonoBehaviour{
    [SerializeField] private ParticleSystem batFlyParticle;

    void Start() {
        StartCoroutine(BatFly());
    }

    IEnumerator BatFly() {
        batFlyParticle.gameObject.SetActive(false);
        batFlyParticle.gameObject.SetActive(true);
        yield return new WaitForSeconds(Random.Range(5f, 25f));
        StartCoroutine(BatFlyNext());
    }

    IEnumerator BatFlyNext() {
        batFlyParticle.gameObject.SetActive(false);
        batFlyParticle.gameObject.SetActive(true);
        yield return new WaitForSeconds(Random.Range(10f, 30));
        StartCoroutine(BatFly());
    }
}
