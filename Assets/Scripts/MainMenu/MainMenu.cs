﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour{
    [SerializeField] private GameObject Menu;
    [SerializeField] private GameObject Setting;
    [SerializeField] private GameObject Ninja;
    [SerializeField] private GameObject TeleportPos;
    [SerializeField] private ParticleSystem teleportPart;
    
    private Animator _animator;

    private void Start()
    {
        _animator = Ninja.GetComponent<Animator>();
    }

    public void StartGame() {
 
  
        _animator.SetInteger("State", 1);
        StartCoroutine(StartGameCor());

    }

    public void LevelGame() {
        SceneManager.LoadScene("LevelManager");
       
    }

    public void SetingGame() {
        Setting.SetActive(true);
        Menu.SetActive(false);
    }

    public void ReturnMenu() {
        Setting.SetActive(false);
        Menu.SetActive(true);
    }

    public void ExitGame() {
        Application.Quit();
    }

    IEnumerator StartGameCor()
    {
        yield return new WaitForSeconds(3.3f);
        Vector2 newPos = new Vector2(TeleportPos.transform.position.x, TeleportPos.transform.position.y);
        Instantiate(teleportPart, newPos, Quaternion.identity);
        Ninja.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("1");
    }
    
    // IEnumerator StartGameCor2()
    // {
    //     yield return new WaitForSeconds(1f);
    //     SceneManager.LoadScene("1");
    // }
    
}
