﻿using System.Collections;
using UnityEngine;

public class BatLoop : MonoBehaviour{
    [SerializeField] private ParticleSystem bat;
    private                  ParticleSystem batPart;

    void Start() {
        batPart = GetComponent<ParticleSystem>();
        StartCoroutine(BatStartLoop());
    }


    IEnumerator BatStartLoop() {
        yield return new WaitForSeconds(Random.Range(7, 12));
        batPart.Play();
        StartCoroutine(BatStartLoop());
    }
}
