﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour{
    private int    level;
    private String lvl;

    private void Start() {
        var levelText = GetComponentInChildren<TextMeshProUGUI>();
        level = Int32.Parse(levelText.text);
    }

    public void NextLevel() {
        SceneManager.LoadScene(level);
    }
    
}
