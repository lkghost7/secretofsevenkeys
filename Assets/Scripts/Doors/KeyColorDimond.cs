﻿using System.Collections;
using Lean.Pool;
using UnityEngine;

public class KeyColorDimond : MonoBehaviour{
    private                  Animator       _animator;
    [SerializeField] private GameObject     DimondKey;
    [SerializeField] private ParticleSystem OpenDoorEffect;
    [SerializeField] private GameObject     DimondKeyForDoor;
    [SerializeField] private AudioSource    SoudDimondEffect;


    private void Start() {
        _animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")) {
            // print("NAYDEN ");
        }

        if(other.CompareTag("DoorDimond")) {
            // print("что попало в тригере алмаза   " + other.gameObject.name);
            DimondKey key = DimondKey.GetComponent<DimondKey>();
            // print(key.keyCodeDoor + "код двери");
            if(key.keyCodeDoor) {
                OpenDoorDimondEffect(transform.position);
                SoudDimondEffect.Play();
                _animator.enabled = true;
                DimondKeyForDoor.gameObject.SetActive(false);
            }
        }
    }

    public void OpenDoorDimondEffect(Vector2 position) //открыть дверь алмазную партикл
    {
        // print("public void OpenDoorDimondEffect(Vector2 position)");
        ParticleSystem openDoorDimond = LeanPool.Spawn(OpenDoorEffect, position, Quaternion.identity);

        StartCoroutine(DespawnParticleEffect(openDoorDimond));
    }

    IEnumerator DespawnParticleEffect(ParticleSystem particleSystem) // деспаунит партиклы
    {
        yield return new WaitForSeconds(3f);
        LeanPool.Despawn(particleSystem);
    }
}
