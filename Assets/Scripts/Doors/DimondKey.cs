﻿using System;
using System.Collections;
using Lean.Pool;
using UnityEngine;

public class DimondKey : MonoBehaviour{
    [SerializeField] private ParticleSystem DimondKeyPickUp;
    [SerializeField] private ParticleSystem DimondKeyEffectStars;
    [SerializeField] private GameObject ColorUIDimondKeyActive;
    [SerializeField] private GameObject ColorUIDimondKeyDeactive;
    [SerializeField] private AudioSource SoundDimond;
    
    
    public                   bool           keyCodeDoor;
    private ParticleSystem dimondStar;
    private void Start() {
       dimondStar= LeanPool.Spawn(DimondKeyEffectStars, transform.position, 
      Quaternion
      .identity);
      // DimondKeyEffectStars.gameObject.SetActive(true);
     
    }

    public void DimondKeyEffect() {
        dimondStar.loop = false;
        StartCoroutine(DespawnParticleEffect(dimondStar));
    }


    public void PickDoorDimondEffect(Vector2 position) //взять алмахный ключ партикл
    {
    SoundDimond.Play();
        ParticleSystem DimondKeyPickUp = LeanPool.Spawn(this.DimondKeyPickUp, position, Quaternion.identity);
      ColorUIDimondKeyDeactive.SetActive(false);
        ColorUIDimondKeyActive.SetActive(true);
        StartCoroutine(DespawnParticleEffect(DimondKeyPickUp));
    }
    
   


    IEnumerator DespawnParticleEffect(ParticleSystem particleSystem) // деспаунит партиклы
    {
        yield return new WaitForSeconds(3f);
        LeanPool.Despawn(particleSystem);
    }
}
