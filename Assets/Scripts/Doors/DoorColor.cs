﻿using UnityEngine;

public class DoorColor : MonoBehaviour
{
    [SerializeField] private GameObject Leverage0;
    [SerializeField] private GameObject Leverage1;
    [SerializeField] private GameObject UiLeverageOn;
    [SerializeField] private GameObject UiLeverageOf;
    [SerializeField] private AudioSource SoundDoor;
    
    

    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
        SoundDoor.Play();
            // print("плеер нажал");
            Leverage0.SetActive(true);
            Leverage1.SetActive(false);
            _animator.enabled = true;
            UiLeverageOn.SetActive(false);
            UiLeverageOf.SetActive(true);
            
        }
    }
}