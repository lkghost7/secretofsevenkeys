﻿using UnityEngine;

public class CheckCell : MonoBehaviour {
    
    
    
    // private void OnTriggerStay2D(Collider2D other) {
    //     if(other.CompareTag("Cell")) {
    //     print("я внтури коллайдера " + other.gameObject.name);
    //         
    //     }
    // }
    
    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Cell")) {
            // print("я внтури коллайдера " + other.gameObject.name);
            GridCell cell = other.GetComponent<GridCell>();
            cell.isEmpty = true;
        }
    }
}
