﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour{
    private GoldCollect _goldCollect;
    private int activeScene;
    private PlayerController _playerController;
    private void Start() {
        _goldCollect           =  FindObjectOfType<GoldCollect>();
        _goldCollect.NextLevel += NextRound;
       activeScene = SceneManager.GetActiveScene().buildIndex;
       _playerController = FindObjectOfType<PlayerController>();
    }

    private void NextRound() {
        
        if(activeScene + 1 == 40) {
            SceneManager.LoadScene(0);
        }
        else {
            StartCoroutine(WaitNextRound());

        }
    }

    IEnumerator WaitNextRound() {
     _playerController.StateEnd();
        // print("WaitNextRound");
        yield return new WaitForSeconds(2.4f);
            SceneManager.LoadScene(activeScene + 1);
    }
}
