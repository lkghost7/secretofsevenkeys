﻿using System;
using System.Collections;
using Lean.Pool;
using UnityEngine;

public class OnEffectDimond : MonoBehaviour
{
    
    [SerializeField] private ParticleSystem EffectDimondPanel;
    
    private void OnEnable() {
        // print("vluch");
        EffectDimondPanel.gameObject.SetActive(true);
        EffectDimondPanel.Play();
        StartCoroutine(ParticleEffect());
    }
    
 
    IEnumerator ParticleEffect() 
    {
        yield return new WaitForSeconds(4f);
        EffectDimondPanel.gameObject.SetActive(false);
      
    }
    
}
