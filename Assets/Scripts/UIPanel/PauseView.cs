﻿using UnityEngine;

public class PauseView : MonoBehaviour{
    [SerializeField] private Camera     MainCamera;
    [SerializeField] private GameObject PausePanel;
    [SerializeField] private GameObject Ninja;
    [SerializeField] private GameObject BackGround;
    [SerializeField] private GameObject Target;

    [SerializeField] private GameObject SetingMenu;
    [SerializeField] private float ViewModeSpeed = 15;


    [SerializeField] private int LeftView = 15;
    [SerializeField] private int RightView = 30;
    [SerializeField] private int DownView = 5;
    [SerializeField] private int UpView = 5;
    

    public bool GetBtnLeft  {get; set;}
    public bool GetBtnRight {get; set;}
    public bool GetBtnUp    {get; set;}
    public bool GetBtnDown  {get; set;}

    public bool mobileBtOn;

    private Vector3 bGSpawn;
    private bool    pauseOn;
    private bool    bottonOf;

    private void Update() {
        // print("камера лок");

        if(!pauseOn) {
            return;
        }

        bottonOf = true;
        if(Input.GetKey(KeyCode.RightArrow)) {
            // print("right");
            Vector3 currientPos   = MainCamera.transform.position;
            Vector3 currientPosBg = BackGround.transform.position;
            if(currientPos.x < Target.transform.position.x + RightView) {
                MainCamera.transform.position =
                    new Vector3(currientPos.x + +ViewModeSpeed * Time.unscaledDeltaTime, currientPos.y, currientPos.z);
                BackGround.transform.position =
                    new Vector3(currientPosBg.x + ViewModeSpeed * Time.unscaledDeltaTime, currientPosBg.y,
                                currientPosBg.z);
                // print("tek pos");
            }
        }

        if(Input.GetKey(KeyCode.LeftArrow)) {
            Vector3 currientPos   = MainCamera.transform.position;
            Vector3 currientPosBg = BackGround.transform.position;
            if(currientPos.x > Target.transform.position.x - LeftView) {
                MainCamera.transform.position =
                    new Vector3(currientPos.x + -ViewModeSpeed * Time.unscaledDeltaTime, currientPos.y, currientPos.z);

                BackGround.transform.position =
                    new Vector3(currientPosBg.x - ViewModeSpeed * Time.unscaledDeltaTime, currientPosBg.y,
                                currientPosBg.z);
            }
        }

        if(Input.GetKey(KeyCode.DownArrow)) {
            Vector3 currientPos   = MainCamera.transform.position;
            Vector3 currientPosBg = BackGround.transform.position;
            if(currientPos.y > Target.transform.position.y - DownView) {
                MainCamera.transform.position =
                    new Vector3(currientPos.x, currientPos.y + -ViewModeSpeed * Time.unscaledDeltaTime, currientPos.z);

                BackGround.transform.position =
                    new Vector3(currientPosBg.x, currientPosBg.y - ViewModeSpeed * Time.unscaledDeltaTime,
                                currientPosBg.z);
            }
        }

        if(Input.GetKey(KeyCode.UpArrow)) {
            Vector3 currientPos   = MainCamera.transform.position;
            Vector3 currientPosBg = BackGround.transform.position;
            if(currientPos.y < Target.transform.position.y + UpView) {
                MainCamera.transform.position =
                    new Vector3(currientPos.x, currientPos.y + ViewModeSpeed * Time.unscaledDeltaTime, currientPos.z);

                BackGround.transform.position =
                    new Vector3(currientPosBg.x, currientPosBg.y + ViewModeSpeed * Time.unscaledDeltaTime,
                                currientPosBg.z);
            }
        }

        if(!mobileBtOn) {
            return;
        }

        // print("Move");
        MoveRightView();
        MoveLeftView();
        MoveDownView();
        MoveUpView();
    }


    public void MoveRightView() {
        if(GetBtnRight) {
            Vector3 currientPos   = MainCamera.transform.position;
            Vector3 currientPosBg = BackGround.transform.position;

            if(currientPos.x < Target.transform.position.x +RightView) {
                MainCamera.transform.position =
                    new Vector3(currientPos.x + +ViewModeSpeed * Time.unscaledDeltaTime, currientPos.y, currientPos.z);
                BackGround.transform.position =
                    new Vector3(currientPosBg.x + ViewModeSpeed * Time.unscaledDeltaTime, currientPosBg.y,
                                currientPosBg.z);
            }
        }
    }

    public void MoveLeftView() {
        if(GetBtnLeft) {
            print("MoveLeftView");
            Vector3 currientPos   = MainCamera.transform.position;
            Vector3 currientPosBg = BackGround.transform.position;
            if(currientPos.x > Target.transform.position.x - LeftView) {
                MainCamera.transform.position =
                    new Vector3(currientPos.x + -ViewModeSpeed * Time.unscaledDeltaTime, currientPos.y, currientPos.z);
                BackGround.transform.position =
                    new Vector3(currientPosBg.x - ViewModeSpeed * Time.unscaledDeltaTime, currientPosBg.y,
                                currientPosBg.z);
            }
        }
    }

    public void MoveDownView() {
        if(GetBtnDown) {
            Vector3 currientPos   = MainCamera.transform.position;
            Vector3 currientPosBg = BackGround.transform.position;
            if(currientPos.y > Target.transform.position.y - DownView) {
                MainCamera.transform.position =
                    new Vector3(currientPos.x, currientPos.y + -ViewModeSpeed * Time.unscaledDeltaTime, currientPos.z);

                BackGround.transform.position =
                    new Vector3(currientPosBg.x, currientPosBg.y - ViewModeSpeed * Time.unscaledDeltaTime,
                                currientPosBg.z);
            }
        }
    }

    public void MoveUpView() {
        if(GetBtnUp) {
            Vector3 currientPos   = MainCamera.transform.position;
            Vector3 currientPosBg = BackGround.transform.position;
            if(currientPos.y < Target.transform.position.y + UpView) {
                MainCamera.transform.position =
                    new Vector3(currientPos.x, currientPos.y + ViewModeSpeed * Time.unscaledDeltaTime, currientPos.z);

                BackGround.transform.position =
                    new Vector3(currientPosBg.x, currientPosBg.y + ViewModeSpeed * Time.unscaledDeltaTime,
                                currientPosBg.z);
            }
        }
    }


    public void PauseON() {
        pauseOn        = true;
        Time.timeScale = 0;
        PausePanel.SetActive(true);
        if(!bottonOf) {
            bGSpawn = BackGround.transform.position;
        }
    }

    public void PauseOF() {
        MainCamera.transform.position = new Vector3(Ninja.transform.position.x, Ninja.transform.position.y,
                                                    MainCamera.transform.position.z);
        BackGround.transform.position = new Vector3(bGSpawn.x, bGSpawn.y, bGSpawn.z);
        Time.timeScale                = 1;
        PausePanel.SetActive(false);
        pauseOn  = false;
        bottonOf = false;
    }

    public void SettingMenuOn() {
        SetingMenu.SetActive(true);
        Time.timeScale = 0;
    }
    
    public void SettingMenuOf() {
        SetingMenu.SetActive(false);
        Time.timeScale = 1;
    }
}
