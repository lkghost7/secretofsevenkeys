﻿using System;
using UnityEngine;

public class CountEffect : MonoBehaviour
{
    [SerializeField] private ParticleSystem countEffecSystem;
    private DestroedGround _destroedGround;


    private void Start()
    {
        _destroedGround = FindObjectOfType<DestroedGround>();
        _destroedGround.CountEffect += StartParticle;
    }

    private void StartParticle()
    {
        countEffecSystem.Play();
        // print("countEffecSystem");
    }
}