﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpRound2 : MonoBehaviour
{
    [SerializeField] private GameObject HelperText;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            HelperText.SetActive(false);
         
        }
    }

}
