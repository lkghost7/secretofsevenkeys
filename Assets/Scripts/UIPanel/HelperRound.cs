﻿using System.Collections;
using UnityEngine;

public class HelperRound : MonoBehaviour
{
    [SerializeField] private GameObject HelperText;
    [SerializeField] private GameObject HelperText2;
    [SerializeField] private GameObject HelperText3;
    [SerializeField] private float timer = 3f;
    
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            HelperText.SetActive(false);
            StartCoroutine(TextOf());

        }
    }

    IEnumerator TextOf()
    {
        yield return new WaitForSeconds(timer);
        HelperText2.SetActive(false);
        HelperText3.SetActive(false);
    }
}
