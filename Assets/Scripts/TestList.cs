﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TestList : MonoBehaviour
{

    private List<String> member ;
    
    private void Start()
    {
        member = new List<string>();
        
        member.Add("Manfodf");
        member.Add("Mxora");
        member.Add("Vasa");
        member.Add("Vasa2");
        member.Add("Manya");
        member.Add("Manya2");
        member.Add("Anna");
        member.Add("Mriko");
        member.Add("Anna2");
        member.Sort();
        
        foreach (var element in member)
        {
           var firsChar = element[0].ToString();

           if (firsChar.Equals("V"))
           {
               print(element);
           }
        
        }
    }
    
    
}
