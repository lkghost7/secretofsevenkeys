﻿using UnityEngine;

public class Paralax2 : MonoBehaviour
{

    [SerializeField] private GameObject Ninja;
    [SerializeField] private GameObject Back1;
    [SerializeField] private GameObject Back2;
    private PlayerController _playerController;

    private void Start()
    {
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void FixedUpdate() {

        if (_playerController.GetBtnRight)
        {
            print("nagal vpravo");
            Vector2 _right = new Vector2(Back1.transform.position.x + 1000f, Back1.transform.position.y);
            Vector2 _centr = new Vector2(Back1.transform.position.x, Back1.transform.position.y);
            // Back1.transform.position = Vector3.MoveTowards(_centr, _right, 0.1f);
            Back1.transform.position = Vector3.Lerp(_centr, _right, 0.006f * Time.deltaTime);
        }
       
         if (_playerController.GetBtnLeft)
         {
             Vector2 _left = new Vector2(Back1.transform.position.x - 1000f, Back1.transform.position.y);
             Vector2 _centr = new Vector2(Back1.transform.position.x, Back1.transform.position.y);
             // Back1.transform.position = Vector3.MoveTowards(_centr, _left, 0.1f);
             Back1.transform.position = Vector3.Lerp(_centr, _left, 0.006f * Time.deltaTime);
                    print("nagal vlevo");
                }
         
         if (_playerController.GetBtnDown)
         {
             print("nagal vniz");
             Vector2 _down = new Vector2(Back1.transform.position.x, Back1.transform.position.y -1000f);
             Vector2 _centr = new Vector2(Back1.transform.position.x, Back1.transform.position.y);
             // Back1.transform.position = Vector3.MoveTowards(_centr, _down, 0.1f);
             Back1.transform.position = Vector3.Lerp(_centr, _down, 0.006f * Time.deltaTime);
         }
       
         if (_playerController.GetBtnUp)
         {
             Vector2 _up = new Vector2(Back1.transform.position.x, Back1.transform.position.y +1000f);
             Vector2 _centr = new Vector2(Back1.transform.position.x, Back1.transform.position.y);
             // Back1.transform.position = Vector3.MoveTowards(_centr, _up, 0.1f);
             Back1.transform.position = Vector3.Lerp(_centr, _up, 0.006f * Time.deltaTime);
             print("nagal up");
         }
         
    }
}
