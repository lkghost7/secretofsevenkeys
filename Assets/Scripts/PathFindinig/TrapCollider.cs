﻿using UnityEngine;

public class TrapCollider : MonoBehaviour
{
    private GridCell positionPlayerCell;

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<GridCell>() != null)
        {
            positionPlayerCell = other.gameObject.GetComponent<GridCell>();
        }
    }

    public GridCell GetCellPlayerPosition()
    {
        if (positionPlayerCell != null)
        {
            return positionPlayerCell;
        }

        return null;
    }
}