﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimerRound : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timeSecond;
    [SerializeField] private TextMeshProUGUI timeMinute;
    [SerializeField] private TextMeshProUGUI RoundNamber;

    private TimeSpan _timeSpan;
    private String activeScene;

    void Start()
    {
        StartCoroutine(TimeRound());
        activeScene = SceneManager.GetActiveScene().buildIndex.ToString();
        // print(activeScene);
        RoundNamber.text = activeScene;
    }

    IEnumerator TimeRound()
    {
        _timeSpan = TimeSpan.FromSeconds(Time.time);
        // print(_timeSpan.Seconds + "  sec");
        // print(_timeSpan.Minutes + " min");
        String timeSec = _timeSpan.Seconds.ToString("00");
        String timeMin = _timeSpan.Minutes.ToString("00");
        timeSecond.text = timeSec;
        timeMinute.text = timeMin;

        yield return new WaitForSeconds(1f);
        StartCoroutine(TimeRound());
    }
}