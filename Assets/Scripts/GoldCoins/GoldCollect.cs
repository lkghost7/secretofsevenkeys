﻿using System;
using TMPro;
using UnityEngine;

public class GoldCollect : MonoBehaviour
{ 
    
    [SerializeField] private TextMeshProUGUI GoldItem;
    private int goldCoinCount;
    private DestroedGround _destroedGround;

    public Action NextLevel = delegate { };
  
    private void Start()
    {
        _destroedGround = FindObjectOfType<DestroedGround>();
        _destroedGround.GetGoldCoins += CheckCountGoldCoins;

    }

    private void CheckCountGoldCoins()
    {
        CollectItem();
        if (goldCoinCount == 1)
        {
           NextLevel(); 
        }
    }

    private void CollectItem()
    {
        if (transform.childCount != null)
        {
            goldCoinCount = transform.childCount;
            GoldItem.text = (goldCoinCount -1).ToString("00");
        }
    }
}