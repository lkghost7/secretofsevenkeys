﻿using UnityEngine;

public class CurrientPositionEnemy : MonoBehaviour{
    private GridCell currientCellEnemy;
    private Vector2  cellPosition;
    private EnemyController _enemyController;

    private void Start() {
        _enemyController = FindObjectOfType<EnemyController>();
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.transform.GetComponent<GridCell>() != null) {
            currientCellEnemy = other.transform.GetComponent<GridCell>();
            cellPosition = currientCellEnemy.transform.position;
        }
    }

    public Vector2 GetCellPositionEnemy() {
        print(cellPosition);
        return cellPosition;
    }

    public GridCell GetCellEnemy() {
        return currientCellEnemy;
    }
}