﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapIceHold : MonoBehaviour
{
    [SerializeField][Tooltip("эффект льда")] private ParticleSystem IceEffect;
    [SerializeField] private AudioSource SoundEffectIce;
    
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy") ||other.CompareTag("Player"))
        {
       SoundEffectIce.Play();
          IceEffect.gameObject.SetActive(true);
        }
    }
}
