﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using UnityEngine;

enum StateEnemy{
    StartState,
    Idle,
    RunToPlayer,
    EnemyDown,
    EnemyDownOfTheRope,
    EnemyInTrap,
    EnemyInTrapUp,
    EnemyInTrapRightLeft,
    EnemyDeadState,
    RespawnState,
    RespawnStateLight,
    WaitState,
}

public class EnemyController : MonoBehaviour{
    [SerializeField] private float speed = 1;
    [SerializeField] private bool  enemyStopForRefresh;
    [SerializeField] private bool  enemyStop;

    [Header("Задать параметры")]
    //Хедер
    [SerializeField]
    private GridCell currientposition;

    [SerializeField]                   private GridCell              endPosition;
    [SerializeField]                   private CurrientPositionEnemy _currientPositionEnemy;
    [SerializeField]                   private GameObject            m_GroundPoint;
    [SerializeField]                   private GameObject            m_RopeChekPoint;
    [SerializeField]                   private GameObject            m_LadderChekPoint;
    [SerializeField]                   private GameObject            m_CharacterEnemy;
    [SerializeField]                   private GameObject            m_TrapChekPoint;
    [SerializeField]                   private GameObject            m_DeadGround;
    [SerializeField]                   private ParticleSystem        skullParticleSystem;
    [SerializeField]                   private ParticleSystem        lightRespParticleSystem;
    [Header("Sound")] [SerializeField] private AudioSource           SoundDeath;
    [SerializeField]                   private AudioSource           SoundTeleport;

    [SerializeField] private AudioSource SoundMobUpal;

    // ---==== Приватные ====--- //
    private int   _enemyCountPoints;
    private int   currientIndex = 0;
    private float futureTime;
    private float futureRefresh;

    private StateEnemy       _currientStateEnemy;
    private PathFinding      _pathFinding;
    private List<GridCell>   waypoints;
    private TrapCollider     _trapCollider;
    private Rigidbody2D      _rb;
    private PlayerController _playerController;
    private DeadEoom         _deadEoom;
    private RespawnPoint     _respawnPoint;
    private Vector2          direction;
    private Vector2          targetDirectionPosition;
    private Vector2          _trapPointUp;
    private Vector2          _trapPointDown;

    private bool pointChaildDown;
    private bool pointChaildUp;
    private bool stopForWait;
    private bool startCorutin;

    private ParticleSystem _skullParticle;
    private ParticleSystem _lightRespParticle;
    private Vector2        _gizmoChekGround;

    // ---==== Property ====--- //

    public bool EnemyStateRope   {get; set;}
    public bool EnemyStateLadder {get; set;}
    public bool EnemyChekGround  {get; set;}
    public bool EnemyChekTrap    {get; set;}
    public bool EnemyLedderDown  {get; set;}

    [Header("задать параметры")] [SerializeField]
    private float RefreshTime = 1f;

    [SerializeField] private float    TimeInTrap = 3f;
    private                  Animator _animator;


    void Start() {
        _trapCollider      = FindObjectOfType<TrapCollider>();
        _pathFinding       = FindObjectOfType<PathFinding>();
        transform.position = currientposition.transform.position;
        _rb                = GetComponent<Rigidbody2D>();
        _playerController  = FindObjectOfType<PlayerController>();
        _deadEoom          = FindObjectOfType<DeadEoom>();
        _respawnPoint      = FindObjectOfType<RespawnPoint>();
        futureRefresh      = Time.time + 1f;
        _animator          = m_CharacterEnemy.GetComponent<Animator>();
    }

    private void ChekDownEnemyAndPlayer() {
        bool playerDownRope = _playerController.PlayerDownRope;
        bool enemyStateRope = EnemyStateRope;
        bool fallingPlayer  = _playerController.FallingPlayer;

        if(enemyStateRope && playerDownRope && fallingPlayer) {
            _currientStateEnemy = StateEnemy.EnemyDownOfTheRope;
        }
    }

    // ---==== UPDATE ====--- //
    void Update() {
        // print(_rb.velocity);
        ChekLadder();
        ChekGround();
        ChekRope();

        if(stopForWait) {
            stopForWait = false;
        }

        // ---==== СТЕЙТ МАШИНА ====--- //

        switch(_currientStateEnemy) {
            case StateEnemy.StartState :

                _currientStateEnemy = StateEnemy.RunToPlayer;
                break;
            // ---==== СТЕЙТ №1 IDLE ====---       -=111111111=- //
            case StateEnemy.Idle :
                EnemyGetCurrientPosition();
                RefreshGrid();
                // print("EnemyIdle:");
                _animator.SetInteger("StateEnemy", 1);

                break;
            // ---==== СТЕЙТ №2 RunToPlayer ====---       -=22222222=- //
            case StateEnemy.RunToPlayer :
                // print(" RunToPlayer");

                if(EnemyStateLadder && !EnemyStateRope && !EnemyLedderDown) {
                    if(EnemyLedderDown) {
                        _animator.SetInteger("StateEnemy", 9);
                        // print(EnemyStateLadder + "787878");
                    }
                    else {
                        _animator.SetInteger("StateEnemy", 5);
                    }
                }

                if(EnemyStateRope && !EnemyStateLadder) {
                    _animator.SetInteger("StateEnemy", 8);
                }

                if(EnemyChekGround && !EnemyStateLadder && !EnemyStateRope) {
                    _animator.SetInteger("StateEnemy", 3);
                }

                if(enemyStop) {
                    return;
                }

                Move();
                ChekDownEnemyAndPlayer();
                ChekGroundLaddeRope();

                break;
            // ---==== СТЕЙТ №3 EnemyDown ====---            -=33333333=-  //
            case StateEnemy.EnemyDown :
                ChekDeadGround();
                ChekTrap();
                // print("EnemyDown");
                _animator.SetInteger("StateEnemy", 4);
                _rb.linearVelocity = new Vector2(0, -5f);

                if(EnemyChekGround) {
                    EnemyGetCurrientPosition();
                    _rb.linearVelocity        = Vector2.zero;
                    _currientStateEnemy = StateEnemy.RunToPlayer;
                }

                if(EnemyStateRope) {
                    EnemyGetCurrientPosition();
                    _rb.linearVelocity        = Vector2.zero;
                    _currientStateEnemy = StateEnemy.RunToPlayer;
                }

                if(EnemyChekTrap) {
                    _rb.linearVelocity    = Vector2.zero;
                    futureTime      = Time.time + TimeInTrap;
                    pointChaildDown = true;
                    // упал в ловушку звук
                    SoundMobUpal.Play();
                    _currientStateEnemy = StateEnemy.EnemyInTrap;
                }

                break;
            // ---==== СТЕЙТ №3.1 EnemyDownOfTheRope ====---               -=3.1 3.1 3.1 3.1=-    //
            case StateEnemy.EnemyDownOfTheRope :
                // print("EnemyDownRope");
                _animator.SetInteger("StateEnemy", 4);
                _rb.linearVelocity = new Vector2(0, -5f);
                if(EnemyChekGround) {
                    EnemyGetCurrientPosition();
                    _rb.linearVelocity        = Vector2.zero;
                    _currientStateEnemy = StateEnemy.RunToPlayer;
                }

                break;
            // ---==== СТЕЙТ №4 EnemyInTrap ====---               -=44444444=-    //

            case StateEnemy.EnemyInTrap :

                _animator.SetInteger("StateEnemy", 2);
                // print("EnemyInTrap:");
                ChekDeadGround();
                MoveInTrapEnemy();
                break;
            // ---==== СТЕЙТ №5 EnemyInTrapUp ====---               -=555555555=-   //
            case StateEnemy.EnemyInTrapUp :
                // print("EnemyInTrapUp");

                float movmentDelta = speed * Time.deltaTime;
                transform.position = Vector2.MoveTowards(transform.position, targetDirectionPosition, movmentDelta);
                if(Mathf.Approximately(transform.position.x, targetDirectionPosition.x)) {
                    if(EnemyChekGround) {
                        if(currientposition.transform.position.x < transform.position.x) {
                            targetDirectionPosition = new Vector2(transform.position.x + 1f, transform.position.y);
                        }
                        else {
                            targetDirectionPosition = new Vector2(transform.position.x - 1f, transform.position.y);
                        }

                        _rb.linearVelocity        = Vector2.zero;
                        _currientStateEnemy = StateEnemy.EnemyInTrapRightLeft;
                    }
                    else {
                        _currientStateEnemy = StateEnemy.EnemyDown;
                    }
                }

                break;
            // ---==== СТЕЙТ №6 EnemyInTrapRightLeft ====---            -=666666666=-  //
            case StateEnemy.EnemyInTrapRightLeft :
                // print("EnemyInTrapRightLeft");
                EnemyGetCurrientPosition();

                transform.position =
                    Vector2.MoveTowards(transform.position, targetDirectionPosition, speed * Time.deltaTime);
                if(Mathf.Approximately(transform.position.x, targetDirectionPosition.x)) {
                    _currientStateEnemy = StateEnemy.EnemyDown;
                }

                break;
            // ---==== СТЕЙТ №7 EnemyDeadState ====---             -=77777777=- //
            case StateEnemy.EnemyDeadState :
                // print("EnemyDeadState");
                enemyStop = true;
                if(!startCorutin) {
                    SoundDeath.Play();
                    StartCoroutine(SkullParticleStart());
                }

                transform.position = _deadEoom.transform.position;

                if(Time.time > futureTime) {
                    futureTime          = Time.time + 1;
                    _currientStateEnemy = StateEnemy.RespawnState;
                }

                break;
            // ---==== СТЕЙТ №8 RespawnState ====---                -=88888888=- //
            case StateEnemy.RespawnState :
                // print("RespawnState");
                startCorutin = false;
                SoundTeleport.Play();
                if(Time.time > futureTime - 1.5f) {
                    StartCoroutine(LightParticleStart());
                    // print("LightParticleStart");
                    _currientStateEnemy = StateEnemy.RespawnStateLight;
                }

                break;

            // ---==== СТЕЙТ №9 RespawnStateLihgt ====---                    -=999999999=- //
            case StateEnemy.RespawnStateLight :
                // print("RespawnStateLight");

                if(Time.time > futureTime) {
                    // transform.position = _respawnPoint.transform.position;
                    transform.position = new Vector2(_respawnPoint.transform.position.x, _respawnPoint.transform
                                                                                                      .position.y - 0.5f
                    );
                    //переключатели для ловушки 
                    pointChaildDown     = true;
                    pointChaildUp       = false;
                    enemyStop           = false;
                    startCorutin        = false;
                    _currientStateEnemy = StateEnemy.EnemyDown;
                }

                break;

            // ---==== СТЕЙТ №10 WaitState ====---                    -=10  10  10  10=- //
            case StateEnemy.WaitState :
                // print("WaitState");
                _animator.SetInteger("StateEnemy", 2);
                _rb.linearVelocity = Vector2.zero;
                break;
        }
    }

    // ---==== Ловушка для врагов ====---//
    private void MoveInTrapEnemy() {
        float movmentDelta = speed * Time.deltaTime;

        if(pointChaildDown) {
            transform.position = Vector2.MoveTowards(transform.position, _trapPointDown, movmentDelta);
            if(Mathf.Approximately(transform.position.x, _trapPointDown.x)
            && Mathf.Approximately(transform.position.y, _trapPointDown.y)) {
                if(Time.time > futureTime) {
                    pointChaildDown = false;
                    pointChaildUp   = true;
                }
            }
        }

        if(pointChaildUp) {
            if(currientposition.transform.position.x < transform.position.x) {
                m_CharacterEnemy.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                targetDirectionPosition               = new Vector2(transform.position.x + 1.1f, transform.position.y);
            }
            else {
                targetDirectionPosition               = new Vector2(transform.position.x - 1.1f, transform.position.y);
                m_CharacterEnemy.transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
            }

            transform.position = Vector2.MoveTowards(transform.position, _trapPointUp, movmentDelta);
            if(Mathf.Approximately(transform.position.x, _trapPointUp.x)
            && Mathf.Approximately(transform.position.y, _trapPointUp.y)) {
                if(Time.time > futureTime + 0.3f) {
                    EnemyGetCurrientPosition();
                    pointChaildUp       = false;
                    pointChaildDown     = false;
                    _currientStateEnemy = StateEnemy.EnemyInTrapUp;
                }
            }
        }
    }

    // ---==== Хелперы  ====---                 //
    private void ScaleForDirection() {
        if(transform.position.x > direction.x) {
            m_CharacterEnemy.transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
        }

        if(transform.position.x < direction.x) {
            m_CharacterEnemy.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        }
    }

    IEnumerator SkullParticleStart() {
        if(skullParticleSystem != null) {
            _skullParticle                    = LeanPool.Spawn(skullParticleSystem);
            _skullParticle.transform.position = transform.position;
            StartCoroutine(WaitSkullParticle());
        }

        startCorutin = true;
        yield return null;
    }

    IEnumerator LightParticleStart() {
        if(lightRespParticleSystem != null) {
            _lightRespParticle                    = LeanPool.Spawn(lightRespParticleSystem);
            _lightRespParticle.transform.position = _respawnPoint.transform.position;
            StartCoroutine(WaitRespParticle());
        }

        // startCorutin = true;
        yield return null;
    }

    IEnumerator WaitRespParticle() {
        yield return new WaitForSeconds(4f);
        LeanPool.Despawn(_lightRespParticle);
    }

    IEnumerator WaitSkullParticle() {
        yield return new WaitForSeconds(3f);
        LeanPool.Despawn(_skullParticle);
    }

    private void ChekGroundLaddeRope() {
        if(!EnemyChekGround && !EnemyStateRope) {
            _currientStateEnemy = StateEnemy.EnemyDown;
        }
    }

    private void EnemyGetCurrientPosition() {
        currientposition = _currientPositionEnemy.GetCellEnemy();
    }

    private void FindPlayer() {
        if(_trapCollider.GetCellPlayerPosition() != null) {
            endPosition = _trapCollider.GetCellPlayerPosition();
        }
    }

    // ---==== Поиск пути ====--- //
    private void Move() {
        ScaleForDirection();
        waypoints = _pathFinding.FindPath(currientposition, endPosition);
        if(enemyStopForRefresh) {
            return;
        }

        float movmentDelta = speed * Time.deltaTime;
        try {
            Vector2 targetPosition = waypoints[currientIndex].transform.position;
            direction          = targetPosition;
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, movmentDelta);
            if(Mathf.Approximately(transform.position.x, targetPosition.x)
            && Mathf.Approximately(transform.position.y, targetPosition.y)) {
                currientposition = waypoints[currientIndex];
                RefreshGrid();
                _enemyCountPoints++;

                if(currientposition == endPosition) {
                    currientIndex     = 0;
                    _enemyCountPoints = 0;
                }
                else {
                    if(waypoints.Count == _enemyCountPoints) {
                        RefreshGrid();
                    }

                    currientIndex++;
                }
            }
        }
        catch(ArgumentOutOfRangeException e) {
            // print("FixError");
            _currientStateEnemy = StateEnemy.Idle;
        }
    }

    private void RefreshGrid() {
        if(Time.time < futureRefresh) {
            return;
        }

        FindPlayer();
        // print("poisk puti");
        // _pathFinding.LoadGrid();
        currientIndex = 0;
        waypoints     = _pathFinding.FindPath(currientposition, endPosition);

        if(waypoints.Count == 0) {
            enemyStopForRefresh = true;
            futureRefresh       = Time.time + RefreshTime;
            _currientStateEnemy = StateEnemy.Idle;
        }
        else {
            enemyStopForRefresh = false;

            _currientStateEnemy = StateEnemy.RunToPlayer;
        }
    }

    public void ChangeStateForDead() {
        futureTime          = Time.time + 2;
        _currientStateEnemy = StateEnemy.EnemyDeadState;
    }

    public void ChangeStateForEnemyDown() {
        _currientStateEnemy = StateEnemy.EnemyDown;
    }


    // ---==== РЕЙКАСТЫ ====--- //
    private void ChekGround() {
        int layerMask = LayerMask.GetMask("Grounds", "Ladder", "EnemyGround", "GroundBridge");

        Vector2      chekGroundPoint = m_GroundPoint.transform.position;
        RaycastHit2D hit             = Physics2D.Raycast(chekGroundPoint, Vector2.down, 0.1f, layerMask);
        if(hit.collider != null) {
            if(hit.collider.CompareTag("LadderAnim")) {
                // print("nahel animation laddder");
                EnemyLedderDown = true;
            }
            else {
                EnemyLedderDown = false;
            }

            if(hit.collider.CompareTag("TrapHold")) {
                // print("trap hols  ");
                _currientStateEnemy = StateEnemy.WaitState;
            }

            _gizmoChekGround = hit.transform.position;
            EnemyChekGround  = true;
        }
        else {
            EnemyChekGround = false;
        }
    }

    private void ChekRope() {
        int          layerMask     = LayerMask.GetMask("Rope");
        Vector2      chekRopePoint = m_RopeChekPoint.transform.position;
        RaycastHit2D hit           = Physics2D.Raycast(chekRopePoint, Vector2.down, 0.3f, layerMask);
        if(hit.collider != null) {
            EnemyStateRope = true;
        }
        else {
            EnemyStateRope = false;
        }
    }

    private void ChekLadder() {
        int          layerMask       = LayerMask.GetMask("Ladder");
        Vector2      chekLadderPoint = m_LadderChekPoint.transform.position;
        RaycastHit2D hit             = Physics2D.Raycast(chekLadderPoint, Vector2.down, 0.2f, layerMask);
        if(hit.collider != null) {
            EnemyStateLadder = true;
        }
        else {
            EnemyStateLadder = false;
        }
    }

    private void ChekDeadGround() {
        int          layerMask     = LayerMask.GetMask("Grounds");
        Vector2      chekRopePoint = m_DeadGround.transform.position;
        RaycastHit2D hit           = Physics2D.Raycast(chekRopePoint, Vector2.down, 0.1f, layerMask);
        if(hit.collider != null) {
            if(hit.collider.CompareTag("DripGround")) {
                ChangeStateForDead();
            }
        }
    }

    private void ChekTrap() {
        int          layerMask     = LayerMask.GetMask("Trap");
        Vector2      chekTrapPoint = m_TrapChekPoint.transform.position;
        RaycastHit2D hit           = Physics2D.Raycast(chekTrapPoint, Vector2.down, 0.2f, layerMask);
        if(hit.collider != null) {
            _trapPointUp   = hit.collider.transform.GetChild(0).transform.position;
            _trapPointDown = hit.collider.transform.GetChild(1).transform.position;

            EnemyChekTrap = true;
        }
        else {
            EnemyChekTrap = false;
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        // Gizmos.DrawWireSphere(transform.position, );
        Vector2 target = new Vector2(m_GroundPoint.transform.position.x, m_GroundPoint.transform.position.y - 0.1f);
        Gizmos.DrawLine(m_GroundPoint.transform.position, target);
    }
}
