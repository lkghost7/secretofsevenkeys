﻿using System.Collections;
using UnityEngine;

public class RopeStay : MonoBehaviour{
    private Vector2          _ropePosition;
    private PlayerController _playerController;

    private void Start() {
        _playerController = FindObjectOfType<PlayerController>();
       
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.CompareTag("RopePoint")) {
            _playerController.LadderStay   = true;
            _playerController.StopUpMove   = true;
            _playerController.RopeStay     = true;
            _playerController.RopeAnim = true;
            _playerController.RopePosition = transform.position;
           
        }
         
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("RopePoint")) {
            _playerController.LadderStay = false;
            _playerController.StopUpMove = false;
            _playerController.RopeStay   = false;
            _playerController.RopeAnim = false;
            StartCoroutine(PlayerDownRope());
        }
        
    }

    IEnumerator PlayerDownRope()
    {
        _playerController.PlayerDownRope = true;
        // print("начал падение");
        yield return  new WaitForSeconds(0.2f);
        // print("упал с веревки");
        _playerController.PlayerDownRope = false;
    }
}
