﻿using UnityEngine;

public class LadderStay : MonoBehaviour{
    private Vector2          _ladderPosition;
    private PlayerController _playerController;

    private void Start() {
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.CompareTag("Player")) {
            _playerController.LadderPosition = transform.position;
            _playerController.LadderStay      = true;
        }
       
    }
    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")) {
            _playerController.LadderStay = false;
        }
    }
}
