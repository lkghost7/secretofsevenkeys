﻿using System.Collections;
using Lean.Pool;
using UnityEngine;

public class DespawnManager : MonoBehaviour{
    public ParticleSystem _groundAnim;
    public ParticleSystem _groundResurect;

    public void StartDespawnPool(ParticleSystem resp) {
        if(resp != null) {
            StartCoroutine(WaitDripRest(resp));
        }
    }

    IEnumerator WaitDripRest(ParticleSystem resp2) {
        // Debug.Log("despawn  !!!!!!!");
        yield return new WaitForSeconds(2f);
        LeanPool.Despawn(resp2);
    }
    

}
