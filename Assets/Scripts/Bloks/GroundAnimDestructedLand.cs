﻿using System;
using System.Collections;
using Lean.Pool;
using UnityEngine;

public class GroundAnimDestructedLand : MonoBehaviour{
    private                  Animator       _animator;
    private                  bool           _explodeGround;
    [SerializeField] private ParticleSystem m_particleGroundDistructed;
    [SerializeField] private AudioSource GroundDestroy;
  
    
    private DespawnManager _despawnManager;
 
    private void Start() {
        _animator = GetComponent<Animator>();
        _despawnManager = FindObjectOfType<DespawnManager>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("DoorDimond") && !_explodeGround) {
            // Debug.Log("взрыв !!!");
            StartCoroutine(DestroedGrounds(this.gameObject));
            _explodeGround = true;
        }
    }

    IEnumerator DestroedGrounds(GameObject gameObject) {
        _animator.enabled = true;
        yield return new WaitForSeconds(1.2f);
        
        GroundDestroy.Play();
         _despawnManager._groundAnim = LeanPool.Spawn(m_particleGroundDistructed, transform.position, Quaternion.identity);
         _despawnManager.StartDespawnPool(_despawnManager._groundAnim);
        gameObject.SetActive(false);
    }

   
}
