﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckUpLadder : MonoBehaviour
{
    private PlayerController _playerController;

    private void Start() {
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.CompareTag("Player")) {
           _playerController.StopUpMove      = true;
            // print("ChekUpLadder вошел");

        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")) {
            _playerController.StopUpMove = false;
            // print("ChekUpLadder вышел");

        }
    }

    // public Vector2 GetLadderPosition() {
    //     {
    //       
    //        return _ladderPosition;
    //     }
    // }
}
