﻿using UnityEngine;

public class PlayerAnim : MonoBehaviour
{
    private Animator _animator;
    private PlayerController _playerController;
    private bool _checkMovePlayer;
    private bool _checkMoveUpPlayer;
  
    private int run = 3;
    private int idle = 1;
    private int idle2 = 2;
    private int falenDown = 4;
    private int ladderRun = 5;
    private int ladderStay = 6;
    private int ropeRun = 7;
    private int ropeStay = 8;
    

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _playerController = FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        if(_playerController.StopAnim) {
            return;
        }
        
        ChekMovePlayerBTN();
        ChekMoveUpPlayerBTN();

        if (_playerController.LadderStay && !_checkMovePlayer && !_playerController.RopeAnim)
        {
                
            if (_playerController.LadderUpAnim)
            {
                _animator.SetInteger("PlayerState", idle2);
            }
            else
            {
                _animator.SetInteger("PlayerState", ladderStay);
            }

            if (!_playerController.LadderUpAnim && _playerController.GetBtnUp || _playerController.GetBtnDown)
            {
                // print("анимация ползания по лестнице");
            _animator.SetInteger("PlayerState", ladderRun);
              }
         
        }

        if (_playerController.RopeAnim)
        {
            _animator.SetInteger("PlayerState", ropeStay);

            if (_playerController.GetBtnLeft || _playerController.GetBtnRight)
            {
                _animator.SetInteger("PlayerState", ropeRun);
            }
        }

        
        if (_playerController.FallingPlayer && !_playerController.LadderStay && !_playerController.LadderUpAnim)
        {
            _animator.SetInteger("PlayerState", falenDown);
        }

        if (_playerController.IsGround() && !_playerController.LadderStay)
        {
            // _checkLadderUMove = false;
            _animator.SetInteger("PlayerState", idle);
            if (_playerController.GetBtnLeft || _playerController.GetBtnRight)
            {
                _animator.SetInteger("PlayerState", run);
                // print("анимация бега на земле");
            }
        }
        
        if (_playerController.LadderStay && !_playerController.RopeAnim && _checkMovePlayer)
        {
            
            _animator.SetInteger("PlayerState", run);
            // print("анимация бега на лестнице");
        }

         if (_playerController.LadderStay && 
             !_playerController.RopeAnim &&
             _checkMoveUpPlayer &&
             _checkMovePlayer && !_playerController.LadderUpAnim)
                {
                    _animator.SetInteger("PlayerState", ladderRun);
                    // print("анимация бега на лестнице2");
                }
        
    }

    private void ChekMovePlayerBTN()
    {
        if (_playerController.GetBtnLeft || _playerController.GetBtnRight)
        {
            _checkMovePlayer = true;
        }
        else
        {
            _checkMovePlayer = false;
        }
    }
    
    private void ChekMoveUpPlayerBTN()
    {
        if (_playerController.GetBtnUp || _playerController.GetBtnDown)
        {
            _checkMoveUpPlayer = true;
        }
        else
        {
            _checkMoveUpPlayer = false;
        }
    }
    
}