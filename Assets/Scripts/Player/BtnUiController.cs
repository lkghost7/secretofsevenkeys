﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BtnUiController : MonoBehaviour{
    private                  PlayerController _playerController;
    private                  PauseView        _pauseView;
    private                  Portal           _portal;
    private                  float            futureTime;
    [SerializeField] private bool             OnKeyboard;
    private                  Rigidbody2D      _rbPlayer;

    private void Start() {
        _playerController = FindObjectOfType<PlayerController>();
        _pauseView        = FindObjectOfType<PauseView>();
        _portal           = FindObjectOfType<Portal>();
        _rbPlayer         = _playerController.GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if(!OnKeyboard) {
            return;
        }

        if(Input.GetKeyDown(KeyCode.LeftArrow) || (Input.GetKeyDown(KeyCode.A))) {
            _playerController.GetBtnLeft = true;
        }

        if(Input.GetKeyUp(KeyCode.LeftArrow) || (Input.GetKeyUp(KeyCode.A))) {
            _playerController.GetBtnLeft = false;
        }

        if(Input.GetKeyDown(KeyCode.RightArrow) || (Input.GetKeyDown(KeyCode.D))) {
            _playerController.GetBtnRight = true;
        }

        if(Input.GetKeyUp(KeyCode.RightArrow) || (Input.GetKeyUp(KeyCode.D))) {
            _playerController.GetBtnRight = false;
        }

        if(Input.GetKeyDown(KeyCode.UpArrow) || (Input.GetKeyDown(KeyCode.W))) {
            _playerController.GetBtnUp = true;
            StartTeleport();
        }

        if(Input.GetKeyUp(KeyCode.UpArrow) || (Input.GetKeyUp(KeyCode.W))) {
            _playerController.GetBtnUp = false;
        }

        if(Input.GetKeyDown(KeyCode.DownArrow) || (Input.GetKeyDown(KeyCode.S))) {
            _playerController.GetBtnDown = true;
        }

        if(Input.GetKeyUp(KeyCode.DownArrow) || (Input.GetKeyUp(KeyCode.S))) {
            _playerController.GetBtnDown = false;
        }

        if(Input.GetKeyDown(KeyCode.Space)) {
            DripGround();
        }

        // if(Input.GetKeyDown(KeyCode.Alpha1)) {
        //     StartTeleport();
        // }
    }

    //  GetBtnLeft
    public void GetBTNLeftDown() {
        _playerController.GetBtnLeft = true;
        _pauseView.GetBtnLeft        = true;
    }

    public void GetBTNLeftUp() {
        _playerController.GetBtnLeft = false;
        _pauseView.GetBtnLeft        = false;
    }

    // GetBtnRight 
    public void GetBTNRightDown() {
        _playerController.GetBtnRight = true;
        _pauseView.GetBtnRight        = true;
    }

    public void GetBTNRightUp() {
        _playerController.GetBtnRight = false;
        _pauseView.GetBtnRight        = false;
    }

    // GetBtnUp
    public void GetBTNUpDown() {
        _playerController.GetBtnUp = true;
        _pauseView.GetBtnUp        = true;
    }

    public void GetBTNUpUp() {
        _playerController.GetBtnUp = false;
        _pauseView.GetBtnUp        = false;
    }

    // GetBtnDown
    public void GetBTNDownDown() {
        _playerController.GetBtnDown = true;
        _pauseView.GetBtnDown        = true;
    }

    public void GetBTNDownUp() {
        _playerController.GetBtnDown = false;
        _pauseView.GetBtnDown        = false;
    }

    // DripGround
    public void DripGround() {
        if(_playerController.DritGroundReady) {
            return;
        }

        _playerController.DripGrounds();
    }

    public void StartTeleport() {
        if(_playerController.DritGroundReady) {
            return;
        }

        if(_rbPlayer.linearVelocity == Vector2.zero && _playerController.IsGround()) {
            StartCoroutine(TeleportStay());
        }

        // if(Time.time > futureTime) {
        // }
    }

    IEnumerator TeleportStay() {
        _playerController.PortalReady = true;
        yield return new WaitForSeconds(0.1f);
        _portal.SetToPortalValue();

        // futureTime = Time.time + 4;
    }

    public void RestartRound() {
        Time.timeScale = 1;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGames() {
        // Application.Quit();
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }
}
