﻿using System;
using System.Collections;
using Lean.Pool;
using UnityEngine;

public class DestroedGround : MonoBehaviour
{
    [SerializeField] private ParticleSystem HolyFlashParticle;
    [SerializeField] private AudioSource SoundCoin;
    

    private Animator anim;
    private KeyColorDimond _keyColorDimond;
    private PlayerController _playerController;

    public Action GetGoldCoins = delegate { };
    public Action CountEffect = delegate { };

    private void Start()
    {
        _keyColorDimond = FindObjectOfType<KeyColorDimond>();
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("ItemRed"))
        {
            DimondKey key = other.GetComponent<DimondKey>();
            key.PickDoorDimondEffect(other.transform.position);
            key.keyCodeDoor = true;
            ParticleSystem particle = other.gameObject.GetComponent<ParticleSystem>();
            SpriteRenderer sprite = other.gameObject.GetComponent<SpriteRenderer>();
            BoxCollider2D box = other.gameObject.GetComponent<BoxCollider2D>();
            key.DimondKeyEffect();
            box.enabled = false;
            sprite.enabled = false;
           }

        if (other.CompareTag("GoldCoin"))
        {
           GameObject goldCoin = other.gameObject;
            GoldCoinsEffectParticle(goldCoin.transform.position);
            CountEffect();
            SoundCoin.Play();
            Destroy(goldCoin);
            GetGoldCoins();
        }

        if (other.CompareTag("LadderAnim"))
        {
            _playerController.LadderUpAnim = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("LadderAnim"))
        {
            _playerController.LadderUpAnim = false;
        }
    }

    private void GoldCoinsEffectParticle(Vector2 position) //эффект подбора монетки
    {
        ParticleSystem holyFlash = LeanPool.Spawn(HolyFlashParticle, position, Quaternion.identity);
        StartCoroutine(DespawnParticleEffect(holyFlash));
    }

    IEnumerator DespawnParticleEffect(ParticleSystem particleSystem) // деспаунит партиклы
    {
        yield return new WaitForSeconds(2f);
        LeanPool.Despawn(particleSystem);
    }
}