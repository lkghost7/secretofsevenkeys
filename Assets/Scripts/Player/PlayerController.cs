﻿using System.Collections;
using Lean.Pool;
using UnityEngine;
using UnityEngine.SceneManagement;

enum StatePlayer
{
    StartRound,
    StartRound1,
    StartRound2,
    StartRound3,
    Run,
    PrepareTeleport,
    Teleport,
    AfterTeleport,
    DripGround,
    DripGround2,
    DripGround3,
    DeadPlayer,
    DeadPlayer2,
    WaitState,
    EndGame,
}

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D _rb;
    private float _horizontal;
    private float _vertical;
    private Vector2 _direction;
    private Vector2 _currientPortal;

    [SerializeField] private GameObject m_GroundPoint;
    [SerializeField] private GameObject m_GroundPoint2;
    [SerializeField] private GameObject m_Character;
    [SerializeField] private GameObject m_ChekDestroedPoint; // на ней висят колайдеры и скрипты
    [SerializeField] private ParticleSystem m_particleBoom;
    [SerializeField] private ParticleSystem m_particleResurectGround;
    [SerializeField] private ParticleSystem m_particleLaserGround;
    [SerializeField] private ParticleSystem m_StartTeleport;
    [SerializeField] private ParticleSystem m_DeadPlayer;
    [SerializeField] private ParticleSystem m_Start1Particle;
    [SerializeField] private ParticleSystem m_Start2Particle;
    [SerializeField] private GameObject m_CheckDeadZone;
    [SerializeField] private GameObject StartPlayerPoint;

    [Header("Sound")] [SerializeField] private AudioSource EffectLigting;
    [SerializeField] private AudioSource SoundeffectLaser;
    [SerializeField] private AudioSource SoundeffectGround;
    [SerializeField] private AudioSource SoundeffectTeleport;
    [SerializeField] private AudioSource SoundDeath;
    [SerializeField] private AudioSource GroundPuf;

    [Header("Laser")] [SerializeField] private GameObject m_LaserPoint;

    public Vector2 RopePosition { get; set; }
    public Vector2 LadderPosition { get; set; }
    public bool LadderStay { get; set; }
    public bool FallingPlayer { get; set; }
    public bool RopeStay { get; set; }
    public bool StopUpMove { get; set; }
    public bool StopMovePlayer { get; set; }
    public bool RopeAnim { get; set; }
    public bool LadderUpAnim { get; set; }
    public bool GroundStay { get; set; }
    public bool StopAnim { get; set; }
    public bool PlayerDownRope { get; set; }

    // public  bool        EnemyHeadStay  {get; set;}
    public bool PortalReady { get; set; }
    private StatePlayer _currientStatePlayer;
    private float futureTime;
    private float futureTimeRound;
    private GameObject groundBlock;
    Vector2 groundBlocknew;

    // private GroundAnimExplode _groundAnimExplode;

    private ParticleSystem _teleportLeanPool;
    private ParticleSystem _teleportLeanPool2;

    [Header("Задать параметры")]
    // параметры плеера
    [SerializeField]
    private float lifeTimeDripGround;

    [SerializeField] private float MAX_SPEED = 5;
    [SerializeField] private float LaserHand = 1.1f;
    [SerializeField] private float WaitTeleportStay = 1f;
    [SerializeField] private float WaitLaserGround = 1.1f;
 
    //проперти
    public bool GetBtnLeft { get; set; }
    public bool GetBtnRight { get; set; }
    public bool GetBtnUp { get; set; }
    public bool GetBtnDown { get; set; }
    public bool DritGroundReady { get; set; }
    public bool SpawnBlokcs { get; set; }
    private bool stopDead;
    private Animator _animator;

    private void Start()
    {
        m_Character.SetActive(false);
        _rb = GetComponent<Rigidbody2D>();
        _animator = m_Character.GetComponent<Animator>();
        _horizontal = 0;
        stopDead = false;
    }

    // ---==== FIXED UPDATE ====--- //
    private void FixedUpdate()
    {
        ChekDeadGroundAndEnemy();
        // ---==== СТЕЙТ МАШИНА ====--- //
        switch (_currientStatePlayer)
        {
            case StatePlayer.StartRound:
                // print("start round1");

                m_Character.SetActive(false);
                futureTimeRound = Time.time + 1f;
                transform.position = StartPlayerPoint.transform.position;
                _currientStatePlayer = StatePlayer.StartRound1;

                break;

            case StatePlayer.StartRound1:
                // print("start round");

                if (Time.time > futureTimeRound)
                {
                    // print("futureTimeRound");
                    EffectLigting.Play();
                    StartRoundParticle1();
                    // print("StartRoundParticle1");
                    _currientStatePlayer = StatePlayer.StartRound2;
                }

                break;

            case StatePlayer.StartRound2:
                // print("start round2");

                if (Time.time > futureTimeRound + 2f)
                {
                    StartRoundParticle2();
                    _currientStatePlayer = StatePlayer.StartRound3;
                }

                break;

            case StatePlayer.StartRound3:
                // print("start round3");

                if (Time.time > futureTimeRound + 2.2f)
                {
                    m_Character.SetActive(true);
                    _rb.gravityScale = 1;
                    _currientStatePlayer = StatePlayer.Run;
                }

                break;

            case StatePlayer.Run:
                // print("PlayerRun");
                CheckDownPlayer();
                if (StopMovePlayer)
                {
                    return;
                }

                if (LadderStay)
                {
                    MoveToLadder();
                    return;
                }

                if (!IsGround())
                {
                    _rb.gravityScale = 1;
                    PlayerDown();
                    return;
                }

                MoveHorizontal();

                break;

            case StatePlayer.PrepareTeleport:
                // print(" СТЕЙТ PrepareTeleport");
                if (stopDead)
                {
                    m_Character.SetActive(false);
                    return;
                }

                SoundeffectTeleport.Play();
                DritGroundReady = true;
                PortalReady = false;
                futureTime = Time.time + 1f;

                if (m_StartTeleport != null)
                {
                    _teleportLeanPool = LeanPool.Spawn(m_StartTeleport);
                    _teleportLeanPool.transform.position = transform.position;
                    StartCoroutine(WaitTeleport());
                }

                stopDead = true;
                m_Character.SetActive(false);
                _currientStatePlayer = StatePlayer.Teleport;

                break;

            // ---==== СТЕЙТ Teleport ====--- //
            case StatePlayer.Teleport:
                // print(" СТЕЙТ Teleport");

                if (Time.time > futureTime)
                {
                    transform.position = _currientPortal;
                    SoundeffectTeleport.Play();
                    if (m_StartTeleport != null)
                    {
                        _teleportLeanPool2 = LeanPool.Spawn(m_StartTeleport);
                        _teleportLeanPool2.transform.position = transform.position;
                        StartCoroutine(WaitTeleport2());
                    }

                    m_Character.SetActive(true);
                    stopDead = false;
                    _currientStatePlayer = StatePlayer.AfterTeleport;
                }

                break;

            case StatePlayer.AfterTeleport:
                DritGroundReady = false;
                _currientStatePlayer = StatePlayer.Run;

                break;

            // ---==== СТЕЙТ DirtGround ====--- //
            case StatePlayer.DripGround:

                DritGroundReady = true;
                // print("DirtGround");
                // StopMovePlayer = false;
                _rb.linearVelocity = Vector2.zero;
                SpawnBlokcs = true;

                groundBlocknew = groundBlock.transform.position;
                StartCoroutine(WaitLaserHand()); // включить анимацию 
                _currientStatePlayer = StatePlayer.DripGround2;

                break;

            // ---==== СТЕЙТ DripGround2 ====--- //
            case StatePlayer.DripGround2:
                // print("DripGround2");

                if (Time.time > futureTime) //ждать попадания лазера по земле
                {
                    StartBoomGroundParticle(); // запустили партикал взрыв земли (откопать блок)
                    // звук взрыва земли тут
                    StartCoroutine(DripTrapGround(groundBlock)); // откапать блок, потом закопать
                    StartCoroutine(
                        WaitRestGround(
                            groundBlocknew)); // запустить карутину чтобы подождать потом создать эффект заростания земли
                    StartCoroutine(WhiteDripReady());
                    _currientStatePlayer = StatePlayer.DripGround3;
                }

                break;

            // ---==== СТЕЙТ DripGround3 ====--- //
            case StatePlayer.DripGround3:
                // print("DripGround2");
                if (Time.time > futureTime + 0.35f)
                {
                    _currientStatePlayer = StatePlayer.Run;
                }

                break;

            // ---==== СТЕЙТ WaitState ====--- //
            case StatePlayer.WaitState:
                // print("WaitState");

                break;

            // ---==== СТЕЙТ WaitState ====--- //
            case StatePlayer.DeadPlayer2:
                // print("DeadPlayer2");
                // print("DeadPlayer2");
                _rb.linearVelocity = Vector2.zero;
                m_Character.SetActive(false);

                break;

            case StatePlayer.DeadPlayer:
                // print("DeadPlayer");
                // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                _rb.linearVelocity = Vector2.zero;
                SoundDeath.Play();
                StartBoomPlayer();
                m_Character.SetActive(false);
                StartCoroutine(StateDeadPlayer());
                _rb.gravityScale = 0;
                _currientStatePlayer = StatePlayer.DeadPlayer2;
                break;

            // ---==== СТЕЙТ EndGame ====--- //
            case StatePlayer.EndGame:
                SoundeffectTeleport.Play();
                // print("EndGame");
                _rb.gravityScale = 0;
                _rb.linearVelocity = Vector2.zero;
                stopDead = true;
                StartRoundParticle3();
                m_Character.SetActive(false);
                _currientStatePlayer = StatePlayer.WaitState;
                break;
        }
    }

    IEnumerator StateDeadPlayer()
    {
        StopUpMove = true;
        // print("dead корутин");
        _rb.linearVelocity = Vector2.zero;
        // m_Character.SetActive(false);
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void StartBoomPlayer() //взорвать плеера
    {
        Vector2 newPositionBoom =
            new Vector2(transform.position.x, transform.position.y);
        ParticleSystem playerBoom = LeanPool.Spawn(m_DeadPlayer, newPositionBoom, Quaternion.identity);
        StartCoroutine(DespawnParticleEffect(playerBoom));
    }

    IEnumerator WhiteDripReady()
    {
        yield return new WaitForSeconds(0.3f);

        DritGroundReady = false;
    }

    //падение плеера вниз
    private void PlayerDown()
    {
        // print("игрок падает вниз");
        if (_rb.linearVelocity.y < -1f)
        {
            _rb.linearVelocity = new Vector2(0, -7f);
        }
    }

    IEnumerator
        WaitRestGround(Vector2 newblockPos) //ждать столько сколько зарастет земля, потом  включить анимацию зарастания
    {
        yield return new WaitForSeconds(lifeTimeDripGround);
        // зарастание земли звук
        GroundPuf.Play();

        StartRestGroundParticle(newblockPos);
    }

    private void StartRestGroundParticle(Vector2 blockPos) // анимация заростания землю
    {
        Vector2 positionBlock =
            new Vector2(blockPos.x + 0f, blockPos.y - 0f);
        ParticleSystem dripRest = LeanPool.Spawn(m_particleResurectGround, positionBlock, Quaternion.identity);
        StartCoroutine(DespawnParticleEffect(dripRest));
    }


    private void StartRoundParticle1() //вызвать эффект появления
    {
        Vector2 newPositionBoom =
            new Vector2(StartPlayerPoint.transform.position.x, StartPlayerPoint.transform.position.y);
        ParticleSystem start1 = LeanPool.Spawn(m_Start1Particle, newPositionBoom, Quaternion.identity);
        StartCoroutine(DespawnParticleEffect(start1));
    }

    private void StartRoundParticle2() //вызвать эффект портала после появления
    {
        Vector2 newPositionBoom =
            new Vector2(StartPlayerPoint.transform.position.x, StartPlayerPoint.transform.position.y);
        ParticleSystem start2 = LeanPool.Spawn(m_Start2Particle, newPositionBoom, Quaternion.identity);
        StartCoroutine(DespawnParticleEffect(start2));
    }

    private void StartRoundParticle3() //вызвать эффект портала с последним итемом
    {
        Vector2 newPositionBoom =
            new Vector2(transform.position.x, transform.position.y);
        ParticleSystem start2 = LeanPool.Spawn(m_Start2Particle, newPositionBoom, Quaternion.identity);
        StartCoroutine(DespawnParticleEffect(start2));
    }

    private void StartBoomGroundParticle() //взорвать землю - анимация
    {
        // Vector2 newPositionBoom =
        //     new Vector2(groundBlock.transform.position.x + 0.5f, groundBlock.transform.position.y - 0.8f);
        // ParticleSystem dripBoom = LeanPool.Spawn(m_particleBoom, newPositionBoom, Quaternion.identity);
        // StartCoroutine(DespawnParticleEffect(dripBoom));
        SoundeffectGround.Play();
        Vector2 newPositionBoom =
            new Vector2(groundBlock.transform.position.x + 0f, groundBlock.transform.position.y - 0.3f);
        ParticleSystem dripBoom = LeanPool.Spawn(m_particleBoom, newPositionBoom, Quaternion.identity);
        StartCoroutine(DespawnParticleEffect(dripBoom));
    }

    IEnumerator DespawnParticleEffect(ParticleSystem particleSystem) // деспаунит партиклы
    {
        yield return new WaitForSeconds(2f);
        LeanPool.Despawn(particleSystem);
    }

    IEnumerator WaitTeleport() //задержка перед телепортом
    {
        yield return new WaitForSeconds(WaitTeleportStay);
        LeanPool.Despawn(_teleportLeanPool);
    }

    IEnumerator WaitTeleport2() //задержка перед телепортом на другой стороне - они в паре
    {
        yield return new WaitForSeconds(WaitTeleportStay);
        LeanPool.Despawn(_teleportLeanPool2);
    }

    IEnumerator WaitLaserHand() // выстрел из руки лазером
    {
        // print("  IEnumerator WaitLaserHand() // выстрел из руки лазером");
        AnimDripGround();
        SoundeffectLaser.Play();
        m_particleLaserGround.gameObject.SetActive(true);
        yield return new WaitForSeconds(LaserHand);
        m_particleLaserGround.gameObject.SetActive(false);
        StopAnim = false;
    }

    // методы для телепорта
    public void PortaActivated(Vector2 endPointPortal)
    {
        // PortalReady = false;
        _currientPortal = endPointPortal;
        _currientStatePlayer = StatePlayer.PrepareTeleport;
    }

    // проверка плеера на землю, если земли нет падаем при условии велосити вниз -1
    private void CheckDownPlayer()
    {
        if (_rb.linearVelocity.y < -1f)
        {
            FallingPlayer = true;
        }
        else
        {
            FallingPlayer = false;
        }
    }

    //капать землю
    public void DripGrounds()
    {
        int layerMask = LayerMask.GetMask("Grounds");
        Vector2 diggerPoint = m_LaserPoint.transform.position;
        RaycastHit2D hit = Physics2D.Raycast(diggerPoint, Vector2.down, 0.2f, layerMask);
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("DripGround"))
            {
                futureTime = Time.time + WaitLaserGround; //задать время что бы подождать пока лазер попадет по земле
                groundBlock = hit.collider.gameObject;

                _currientStatePlayer = StatePlayer.DripGround;
            }
        }
    }

    private void AnimDripGround()
    {
        StopAnim = true;
        _animator.SetInteger("PlayerState", 9);
        // print("капаю землю анимация");
    }

    //откапать землю, потом зарости 
    IEnumerator DripTrapGround(GameObject gameObject)
    {
        gameObject.SetActive(false);

        yield return new WaitForSeconds(lifeTimeDripGround);
        gameObject.SetActive(true);
    }

    // проверка на землю
    public bool IsGround()
    {
        int layerMask = LayerMask.GetMask("Grounds", "EnemyGround", "BlockMost");
        Vector2 chekGroundPoint = m_GroundPoint.transform.position;
        Vector2 chekGroundPoint2 = m_GroundPoint2.transform.position;
        chekGroundPoint.y -= 0.1f;
        chekGroundPoint2.y -= 0.1f;
        RaycastHit2D hit = Physics2D.Raycast(chekGroundPoint, Vector2.down, 0.1f, layerMask);
        RaycastHit2D hit2 = Physics2D.Raycast(chekGroundPoint2, Vector2.down, 0.1f, layerMask);
        if (hit.collider || hit2.collider != null)
        {
            return true;
        }

        return false;
    }

    // метод для проверки на смерть от ямы и энеми
    private void ChekDeadGroundAndEnemy()
    {
        if (stopDead)
        {
            return;
        }

        int layerMask = LayerMask.GetMask("Grounds", "Enemy");
        Vector2 chekRopePoint = m_CheckDeadZone.transform.position;
        RaycastHit2D hit = Physics2D.Raycast(chekRopePoint, Vector2.down, 0.5f, layerMask);
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("DripGround"))
            {
                _currientStatePlayer = StatePlayer.DeadPlayer;
                stopDead = true;
            }

            if (hit.collider.CompareTag("Enemy"))
            {
                _currientStatePlayer = StatePlayer.DeadPlayer;
                stopDead = true;
            }
        }
    }

    //движение по лестнице
    private void MoveToLadder()
    {
        print("MoveToLadder");
        _rb.gravityScale = 0;
        _rb.linearVelocity = Vector2.zero;
        MoveRight();
        MoveLeft();
        MoveUp();
        MoveDown();
    }

    //движение по горизонтали
    private void MoveHorizontal()
    {
        _rb.gravityScale = 1;
        _rb.linearVelocity = Vector2.zero;
        MoveRight();
        MoveLeft();
    }

    // методы на движение по сторонам 
    public void MoveLeft()
    {
        if (GetBtnLeft)
        {
            print("GetBtnLeft");
            _rb.linearVelocity = new Vector2(-1f * MAX_SPEED, 0);

            if (RopeAnim)
            {
                m_Character.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            }
            else
            {
                m_Character.transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
            }

            if (RopeStay)
            {
                print("    if (RopeStay)");
                Vector2 target = new Vector2(transform.position.x, RopePosition.y - 0.5f);
                transform.position = Vector2.MoveTowards(transform.position, target, 0.09f);
            }
        }
    }



    public void MoveRight()
    {
        if (GetBtnRight)
        {
            _rb.linearVelocity = new Vector2(1f * MAX_SPEED, 0);

            if (RopeAnim)
            {
                m_Character.transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
            }
            else
            {
                m_Character.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            }

            if (RopeStay)
            {
                Vector2 target = new Vector2(transform.position.x, RopePosition.y - 0.5f);
                transform.position = Vector2.MoveTowards(transform.position, target, 0.09f);
            }
        }
    }

    public void MoveUp()
    {
        if (StopUpMove)
        {
            return;
        }

        if (GetBtnUp)
        {
            Vector2 target = new Vector2(LadderPosition.x + 0.5f, transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, target, 0.09f);
            _rb.linearVelocity = new Vector2(0, 1f * MAX_SPEED);
        }
    }

    public void MoveDown()
    {
        if (GetBtnDown)
        {
            Vector2 target = new Vector2(LadderPosition.x + 0.5f, transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, target, 0.09f);
            _rb.linearVelocity = new Vector2(0, -1f * MAX_SPEED);
        }
    }

    public void StateEnd()
    {
        _currientStatePlayer = StatePlayer.EndGame;
    }
}